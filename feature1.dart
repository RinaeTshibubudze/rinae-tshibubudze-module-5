import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dashboard.dart';

class FeaturePage1 extends StatefulWidget {
  const FeaturePage1({Key? key}) : super(key: key);

  @override
  _FeaturePage1State createState() => _FeaturePage1State();
}

class _FeaturePage1State extends State<FeaturePage1> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();
    TextEditingController emailController = TextEditingController();

    Future _AddProfileData() {
      final name = nameController.text;
      final surname = surnameController.text;
      final email = emailController.text;

      final save = FirebaseFirestore.instance.collection("Profile").doc();

      return save
          .set(
              {"Name": name, "Surname": surname, "email": email, "id": save.id})
          .then((value) => log("Profile added!"))
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const DashboardPage()));
            }),
        title: const Text('Feature 1'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.menu), onPressed: () {}),
        ],
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                      Color.fromARGB(102, 12, 87, 98),
                      Color.fromARGB(153, 85, 102, 101),
                      Color.fromARGB(173, 12, 128, 196),
                      Color.fromARGB(255, 6, 95, 151),
                    ])),
                child: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: <Widget>[
                      const Text(
                        "Add data to the database",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                          child: TextField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular((20))),
                                  hintText: "Please type a Name"))),
                      Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                          child: TextField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular((20))),
                                  hintText: "Please type a Surname"))),
                      Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                          child: TextField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular((20))),
                                  hintText: "Please type Email"))),
                      ElevatedButton(
                          onPressed: () {
                            _AddProfileData();
                          },
                          child: Text("Save Data"))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(height: 50.0),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const DashboardPage()));
        },
        tooltip: 'Dashboard Page',
        child: const Icon(Icons.pages),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
