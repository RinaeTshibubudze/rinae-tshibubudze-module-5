import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'login.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyDhFS8fWPJ1GlMdanLPG7GywZFtUx2ir7U",
      appId: "1:952024929628:android:9c146c5e8d0ba4360b1d99",
      messagingSenderId: "952024929628",
      projectId: "mtnflutter",
    ),
  );
  runApp(const Mod4Ass());
}

class Mod4Ass extends StatelessWidget {
  const Mod4Ass({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: AnimatedSplashScreen(
          splash: Image.asset(
            'assets/vharhee_logo.png',
          ),
          nextScreen: const LoginPage(),
          duration: 4000,
          splashTransition: SplashTransition.scaleTransition,
          splashIconSize: 500,
          backgroundColor: Colors.deepOrangeAccent,
        ),
        theme: ThemeData(
            primarySwatch: Colors.deepOrange,
            accentColor: /*  */ Colors.deepOrange,
            scaffoldBackgroundColor: Colors.deepOrange));
  }
}
